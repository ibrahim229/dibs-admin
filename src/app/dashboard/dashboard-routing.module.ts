import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { CategoriesComponent } from '../pages/categories/categories.component';
import { BoxesComponent } from '../pages/boxes/boxes.component';

const routes: Routes = [{
  path: '', component: DashboardComponent, children: [
    { path: "categories", component: CategoriesComponent },
    { path: "boxes", component: BoxesComponent },
    { path: "", redirectTo: "boxes" }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
