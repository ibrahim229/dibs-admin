import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { CategoriesComponent } from '../pages/categories/categories.component';
import { AddEditCategoryComponent } from '../pages/categories/add-edit-category/add-edit-category.component';
import { SharedModule } from '../shared/shared.module';
import { BoxesComponent } from '../pages/boxes/boxes.component';
import { AddEditBoxComponent } from '../pages/boxes/add-edit-box/add-edit-box.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatIconModule} from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { environment } from "../../environments/environment";
import {
  AngularFireStorageModule,
} from "@angular/fire/storage";
import { ImageCropperModule } from 'ngx-image-cropper';
import { AngularFireModule } from "@angular/fire";
@NgModule({
  declarations: [DashboardComponent,
    CategoriesComponent, AddEditCategoryComponent,
    BoxesComponent, AddEditBoxComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    NgbModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDlxDemVKFijlTpaOxrmNJsCWlxOqU2z3g'
    }),
    MatProgressSpinnerModule,
    MatSelectModule,
    MatListModule,
    MatSlideToggleModule,
    MatIconModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, "cloud"),
    ImageCropperModule
  ],
  providers: []
})
export class DashboardModule { }
