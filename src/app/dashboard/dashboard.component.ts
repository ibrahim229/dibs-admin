import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userType: string;
  constructor(public router: Router) { }

  ngOnInit(): void {
    this.userType = localStorage.getItem("boxOwner") ? "boxOwner" : "admin";
    console.log("User type", this.userType);
  }

  Logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("boxOwner");
    this.router.navigate(['login']);
  }

}
