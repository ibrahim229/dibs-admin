export type Category = {
    name: string;
    nameAR: string;
    boxes: Array<string>;
    id?: number;
};