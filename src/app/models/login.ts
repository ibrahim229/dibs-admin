export type LoginAdmin = {
  username: string;
  password: string;
  type: string;
};