export type Box = {
    Name: string;
    Username: string;
    Password: string;
    MinBoxes: Number;
    Long: number;
    Lat: number;
    From: string;
    To: string;
    Price: number;
    Banner: string;
    Logo: string;
    Contact: string;
    Description: string;
    id?: number,
    Type: string
}; 