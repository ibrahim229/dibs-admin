import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { LoginAdmin } from '../models/login';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: LoginAdmin = {
    username: "",
    password: "",
    type: "admin"
  };
  userType: string = "admin";

  loading: boolean = false;
  constructor(private authService: AuthService, public router: Router) { }

  ngOnInit(): void {
  }
  LoginAdmin(admin: NgForm) {
    console.log(this.userType);
    this.loading = true;
    console.log(admin.value)
    if (admin.valid) {
      this.login.type = this.userType;
      this.authService.login(this.login).subscribe(response => {
        console.log("login response");
        console.log(response);
        localStorage.setItem("token", response["token"]);
        if (this.userType == "BoxOwner") {
          localStorage.setItem("boxOwner", response["id"]);
        }
        this.router.navigate(['dashboard']);
        this.loading = false

      }, error => {
        this.loading = false
      })
    }
  }

}
enum UserType {
  admin,
  boxOwner
}