import { Injectable } from '@angular/core';
import { Box } from '../models/box';
import { HttpClient } from '@angular/common/http';
import { Constants } from "./constants"
@Injectable({
  providedIn: 'root'
})
export class BoxsService {

  constructor(private http: HttpClient) { }

  addBox(data: Box) {
    // console.log("final box", data)
    data = {
      Name: data.Name,
      Username: data.Username,
      Password: data.Password,
      MinBoxes: data.MinBoxes,
      Long: data.Long,
      Lat: data.Lat,
      From: data.From,
      To: data.To,
      Price: data.Price,
      Banner: data.Banner,
      Logo: data.Logo,
      Contact: data.Contact.toString(),
      Description: data.Description,
      Type: data.Type
    };
    console.log("final box", data)

    return this.http.post(Constants.domain + '/admin/box', data);
  }

  editBox(data: Box) {
    console.log("comming box", data)

    let box = {
      Name: data.Name,
      Username: data.Username,
      Password: data.Password,
      MinBoxes: data.MinBoxes,
      Long: data.Long,
      Lat: data.Lat,
      From: data.From,
      To: data.To,
      Price: data.Price,
      Banner: data.Banner,
      Logo: data.Logo,
      Contact: data.Contact.toString(),
      Description: data.Description
    };
    console.log(box)
    console.log("edit box")
    return this.http.put(Constants.domain + `/admin/box/${data.id}`, box);
  }
  deleteBox(data: any) {
    console.log(data);
    return this.http.delete(Constants.domain + `/admin/box/${data._id}`, { responseType: 'text' });
  }

  getAllBoxes() {
    return this.http.get(Constants.domain + "/admin/boxes")
  }

  activeBox(boxId: string, isActive: boolean) {
    let body = {
      "isActive": isActive
    }
    return this.http.put(Constants.domain + `/admin/box/${boxId}/activation`, body)
  }
}
