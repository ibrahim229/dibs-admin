import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginAdmin } from '../models/login';
import { Constants } from "./constants"
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthantcated: boolean = false;

  constructor(private http: HttpClient) { }

  login(data: LoginAdmin) {
    data = { username: data.username, password: data.password, type: data.type };
    console.log(data);
    const url = data.type == "BoxOwner" ? "/box/signin" : "/admin/signin";

    return this.http.post(Constants.domain + url, data);
  }

  public getAuthState(): Boolean {
    return this.isAuthantcated;
  }
  public setAuthState(state: boolean): void {
    this.isAuthantcated = state;
  }
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    if (token != null) {
      return true
    } else {
      return false
    }
  }
}
