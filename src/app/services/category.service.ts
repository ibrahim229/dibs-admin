import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from "./constants"
import { Category } from '../models/category';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  addCategory(category: Category) {
    return this.http.post(Constants.domain + '/admin/cateogory', category)
  }
  editCategory(category: Category) {
    console.log(category)
    console.log("edit category")
    let editCategory = {
      name: category.name,
      boxes: category.boxes,
      nameAR: category.nameAR
    }
    return this.http.put(Constants.domain + `/admin/cateogory/${category.id}`, editCategory, { responseType: 'text' });
  }
  deleteCategory(data: any) {
    console.log(data);
    return this.http.delete(Constants.domain + `/admin/cateogory/${data._id}`, { responseType: 'text' });
  }
  getAllCategories() {
    return this.http.get(Constants.domain + "/admin/cateogories")
  }
  firstCategory(categoryId: string, isFirst: boolean) {
    let body = {
      "isFirst": isFirst
    }
    return this.http.put(Constants.domain + `/admin/cateogory/${categoryId}`, body)
  }
}
