import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorDialogComponent } from '../error-dialog/errordialog.component';
import { MatDialogModule } from '@angular/material/dialog'
import { AppProgressSpinnerDialogComponent } from "../app-progress-spinner-dialog/app-progress-spinner-dialog.component";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [ErrorDialogComponent,
    AppProgressSpinnerDialogComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatProgressSpinnerModule,


  ],
  entryComponents: [
    ErrorDialogComponent,
    AppProgressSpinnerDialogComponent
  ]
})
export class SharedModule { }
