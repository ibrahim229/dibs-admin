import { Component, OnInit, ViewChild, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { Category } from 'src/app/models/category';
import { FormControl, NgForm } from '@angular/forms';
import { BoxsService } from 'src/app/services/boxs.service';
import { CategoryService } from 'src/app/services/category.service';


@Component({
  selector: 'app-add-edit-category',
  templateUrl: './add-edit-category.component.html',
  styleUrls: ['./add-edit-category.component.css']
})
export class AddEditCategoryComponent implements OnInit, OnChanges {
  @ViewChild('closebutton') closebutton;
  @Output() categoryCreated: EventEmitter<string> = new EventEmitter()
  @Output() setSelectedTonull: EventEmitter<string> = new EventEmitter()

  category: Category = {
    name: "",
    nameAR: "",
    boxes: []
  }
  @Input() edit: boolean;
  @Input() selectedCategory: any;
  public boxes: any;
  public loading: boolean = false;

  public selectedBoxes = new FormControl();
  constructor(private boxService: BoxsService, private categoryService: CategoryService) {
    this.boxService.getAllBoxes().subscribe((res) => {
      console.log(res);
      this.boxes = res;
    })
  }

  ngOnInit(): void {
  }
  save(form: NgForm) {

    if (this.edit) {
      if (form.valid) {
        this.categoryService.editCategory(this.category).subscribe((res) => {
          console.log(res)
          this.loading = false
          this.closebutton.nativeElement.click();
          this.categoryCreated.emit()
        });
      }
    } else {
      console.log(this.category)
      this.loading = true;
      if (form.valid) {
        this.categoryService.addCategory(this.category).subscribe((res) => {
          console.log(res)
          this.loading = false
          this.closebutton.nativeElement.click();
          this.categoryCreated.emit()
        });
      }
    }
  }
  ngOnChanges() {

    if (this.selectedCategory != null) {
      debugger
      this.category.name = this.selectedCategory.name
      this.category.nameAR = this.selectedCategory.nameAR
      let boxesId = []
      for (let i = 0; i < this.selectedCategory.boxes.length; i++) {
        boxesId.push(this.selectedCategory.boxes[i]._id)
      }
      this.category.boxes = boxesId
      this.category.id = this.selectedCategory._id


    }

  }
  close() {
    this.category.name = ""
    this.category.boxes = []
    this.setSelectedTonull.emit()
  }
}
