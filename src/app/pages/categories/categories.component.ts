import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  public categories: any;
  selected = null
  edit: boolean = false;
  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe((res) => {
      this.categories = res
      console.log(this.categories)
    })
  }
  getCategories(value: string) {

    this.categoryService.getAllCategories().subscribe((res) => {
      this.categories = res
    })
  }
  openEdit(category: any) {
    console.log(category);
    this.selected = category;
    this.edit = true
  }
  setToNull() {
    this.selected = null
    this.edit = false
  }
  deleteCategory(category: any) {
    this.categoryService.deleteCategory(category).subscribe((res) => {
      this.categoryService.getAllCategories().subscribe((res) => {
        console.log(res);
        this.categories = res;
      })
    })
  }
  firstCategory(event, category) {
    console.log(event)
    console.log(category)

    this.categoryService.firstCategory(category._id, event.checked).subscribe(res => {
      console.log(res)
    })
  }
}
