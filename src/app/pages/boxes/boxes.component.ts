import { Component, OnInit, ViewChild } from '@angular/core';
import { BoxsService } from 'src/app/services/boxs.service';
import { AddEditBoxComponent } from './add-edit-box/add-edit-box.component';
import { AngularFireStorage } from "@angular/fire/storage";

@Component({
  selector: 'app-boxes',
  templateUrl: './boxes.component.html',
  styleUrls: ['./boxes.component.css']
})
export class BoxesComponent implements OnInit {

  public boxes: any;
  constructor(private boxService: BoxsService, private storage: AngularFireStorage) { }
  selected = null
  edit: boolean = false;
  boxOwnerData: string;
  ngOnInit(): void {
    this.boxService.getAllBoxes().subscribe((res) => {
      console.log(res);
      this.boxes = res;
      this.boxOwnerData = localStorage.getItem("boxOwner");
      if (this.boxOwnerData && this.boxOwnerData != "" && this.boxOwnerData != "undefined") {
        this.boxes = this.boxes.filter((box) => box._id == this.boxOwnerData);
      }
    })
  }
  openEdit(box: any) {
    console.log(box);
    this.selected = box;
    this.edit = true
  }

  getBoxes(value: string) {
    this.boxService.getAllBoxes().subscribe((res) => {
      this.boxes = res;
      console.log(res);
    })
  }
  setToNull() {
    console.log("Set to null");
    this.selected = null
    this.edit = false
  }
  deleteBox(box: any) {
    // this.deleteImage(box.logo)
    // this.deleteImage(box.banner)
    this.boxService.deleteBox(box).subscribe((res) => {
      this.boxService.getAllBoxes().subscribe((res) => {
        console.log(res);
        this.boxes = res;
      })
    })
  }
  deleteImage(url: string) {
    this.storage.storage.refFromURL(url).delete()
  }

  activeBox(event, box) {
    console.log(event)
    console.log(box)

    this.boxService.activeBox(box._id, event.checked).subscribe(res => {
      console.log(res)
    })
  }
}
