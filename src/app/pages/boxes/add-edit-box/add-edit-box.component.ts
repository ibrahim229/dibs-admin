import { Component, OnInit, ViewChild, Output, EventEmitter, Input, ChangeDetectionStrategy, OnChanges, ElementRef } from '@angular/core';
import { Box } from 'src/app/models/box';
import { LatLngLiteral } from '@agm/core';
import { MatDialog } from "@angular/material/dialog";
import { NgForm } from '@angular/forms';
import { BoxsService } from 'src/app/services/boxs.service';
import { Observable } from 'rxjs';
import { map, finalize } from "rxjs/operators";
import { AngularFireStorage } from "@angular/fire/storage";
@Component({
  selector: 'app-add-edit-box',
  templateUrl: './add-edit-box.component.html',
  styleUrls: ['./add-edit-box.component.css'],
})
export class AddEditBoxComponent implements OnInit, OnChanges {
  @ViewChild('closebutton') closebutton;
  @ViewChild('logoPhoto') logoPhoto;
  @ViewChild('bannerPhoto') bannerPhoto;
  @Output() boxCreated: EventEmitter<string> = new EventEmitter()
  @Output() setSelectedTonull: EventEmitter<string> = new EventEmitter()
  @Input() edit: boolean;
  @Input() selectedBox: any;

  selectedFile: File = null;

  logoDownloadURL: Observable<string>;
  bannerDownloadURL: Observable<string>;
  previousLogo: string;
  previousBanner: string;
  editLogo: boolean = false;
  editBanner: boolean = false;
  from = { hour: 1, minute: 0 };
  to = { hour: 1, minute: 0 };
  box: Box = {
    Name: "",
    Username: "",
    Password: "",
    MinBoxes: 0,
    Long: -49.8046873,
    Lat: -23.8779431,
    From: "1",
    To: "1",
    Price: 0,
    Banner: null,
    Logo: null,
    Contact: "",
    Description: "",
    Type: null
  }

  zoom: number = 15;
  addLocation: boolean = false;
  loading: boolean = false;
  lat = -23.8779431;
  lng = -49.8046873;
  currentPosition;
  constructor(private dialog: MatDialog, private boxService: BoxsService, private storage: AngularFireStorage) {
    if (navigator) {
      navigator.geolocation.getCurrentPosition(pos => {
        this.box.Long = pos.coords.longitude;
        this.box.Lat = pos.coords.latitude;
        this.lat = pos.coords.latitude;
        this.lng = pos.coords.longitude
        this.currentPosition = pos
        console.log(pos);
      }, err => { }, { enableHighAccuracy: true });
    }
  }

  ngOnInit(): void {
    console.log(this.edit)
    console.log(this.selectedBox)
  }

  locationChnaged(location: LatLngLiteral) {
    console.log(location);
    this.box.Lat = location.lat;
    this.box.Long = location.lng;
  }

  toChange(event) {
    console.log(event)
    let date: Date = new Date();
    date.setHours(event.hour, event.minute)
    this.box.To = date.toISOString()
    console.log(this.box.To)
  }
  fromChange(event) {
    console.log(event)
    let date: Date = new Date();
    date.setHours(event.hour, event.minute)
    this.box.From = date.toISOString()
    console.log(this.box.From)
  }

  error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  }
  openMap() {
    this.addLocation = !this.addLocation;
  }
  save(box: NgForm) {
    this.loading = true;
    console.log(box.value)
    if (this.edit) {
      if (box.valid) {
        console.log("valid")
        this.boxService.editBox(this.box).subscribe((res) => {
          console.log(res)
          this.loading = false
          this.closebutton.nativeElement.click();
          this.boxCreated.emit("new box");

        })
      }
    } else {
      if (box.valid) {
        console.log("valid")
        this.boxService.addBox(this.box).subscribe((res) => {
          console.log(res)
          this.loading = false
          this.closebutton.nativeElement.click();
          this.boxCreated.emit("new box");
        })
      }
    }



  }

  typeChange(type: string) {
    console.log(type)
    this.box.Type = type;
  }
  createEdit() {

    this.box.Name = this.selectedBox.name
    this.box.id = this.selectedBox._id
    this.box.Price = this.selectedBox.price
    this.box.Long = this.selectedBox.location.coordinates[1]
    this.box.Logo = this.selectedBox.logo
    this.box.Lat = this.selectedBox.location.coordinates[0]
    this.box.Description = this.selectedBox.description
    this.box.Contact = this.selectedBox.contact,
      this.box.Username = this.selectedBox.username,
      this.box.MinBoxes = this.selectedBox.minBoxes
    this.box.Type = this.selectedBox.type;
    this.box.Banner = this.selectedBox.banner
    this.lat = this.selectedBox.location.coordinates[1]
    this.lng = this.selectedBox.location.coordinates[0]
    this.previousLogo = this.selectedBox.logo
    this.previousBanner = this.selectedBox.banner
    console.log(this.box)
  }
  ngOnChanges() {

    if (this.selectedBox != null) {
      this.createEdit()
      this.addLocation = true
      let from = { hour: 1, minute: 0 }
      from.hour = new Date(this.selectedBox.from).getHours()
      from.minute == new Date(this.selectedBox.from).getMinutes()
      this.from = from;
      let to = { hour: 1, minute: 0 }
      to.hour = new Date(this.selectedBox.to).getHours()
      to.minute = new Date(this.selectedBox.to).getMinutes()
      this.to = to;
    }

  }
  close() {
    this.logoPhoto.nativeElement.value = "";
    this.bannerPhoto.nativeElement.value = "";
    this.addLocation = false
    this.box = {
      Name: "",
      Username: "",
      Password: "",
      MinBoxes: 0,
      Long: this.currentPosition ? this.currentPosition.coords.longitude : -49.8046873,
      Lat: this.currentPosition ? this.currentPosition.coords.latitude : -23.8779431,
      From: "",
      To: "",
      Price: 0,
      Banner: null,
      Logo: null,
      Contact: "",
      Description: "",
      Type: null
    }
    this.from = { hour: 1, minute: 0 }
    this.to = { hour: 1, minute: 0 };
    this.lng = this.currentPosition ? this.currentPosition.coords.longitude : -49.8046873;
    this.lat = this.currentPosition ? this.currentPosition.coords.latitude : -23.8779431;
    this.setSelectedTonull.emit()
    this.editBanner = false
    this.editLogo = false
  }

  onLogoSelected(event) {
    console.log(event.target.files)
    if (this.edit) {
      this.editLogo = true;
    }
    const randomId = Math.random().toString(36).substring(2);
    const file = event.target.files[0];
    const filePath = `logo/${randomId}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`logo/${randomId}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.logoDownloadURL = fileRef.getDownloadURL();
          this.logoDownloadURL.subscribe(url => {

            console.log(url)
            this.box.Logo = url;


          });
        })
      ).subscribe(url => {
        if (url) {
          console.log("one")
        }
      });


  }
  onBannerSelected(event) {
    if (this.edit) {
      this.editBanner = true
    }
    const randomId = Math.random().toString(36).substring(2);
    const file = event.target.files[0];
    const filePath = `banner/${randomId}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`banner/${randomId}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.bannerDownloadURL = fileRef.getDownloadURL();
          this.bannerDownloadURL.subscribe(url => {
            console.log("url", url)
            this.box.Banner = url
          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log("one")
        }
      });
  }
}

