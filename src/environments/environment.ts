// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCHVH4cssAEA7_ihyEcHZ_kAnIQ5y-Ri1U",
    authDomain: "dibs-b85f1.firebaseapp.com",
    databaseURL: "https://dibs-b85f1.firebaseio.com",
    projectId: "dibs-b85f1",
    storageBucket: "dibs-b85f1.appspot.com",
    messagingSenderId: "329324043466",
    appId: "1:329324043466:web:ca10721f52084542902833",
    measurementId: "G-TZWQT2RVTG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
